Graphismes :
Personnage :
Persona 3 par Atlus
Persona 3 FES par Atlus
Persona 4 par Atlus
Persona 4 golden par Atlus
Persona 5 par Atlus
Persona 5 royal par Atlus
Persona Q2: New Cinema Labyrinth par Atlus
\#
Bouton sur Craftpix :
https://craftpix.net/
Plateau de Monopoly:
https://www.monopolypedia.fr/editions/france/classique2014/monopoly-classique-plateau.jpg
Image faillite :
https://www.toute-la-franchise.com/images/zoom/faillite-190919.jpg
Pluie d'argent :
https://i.makeagif.com/media/7-10-2015/R8Q1KO.gif
Maison :
https://www.vippng.com/png/detail/107-1073246_house-logo-green-clip-art-house-green-clip.png
\#
Musique :
Poem for Everyone's Souls de Shoji Meguro
Reach Out To The Truth de Shoji Meguro
Mass Destruction par Lotus Juice, Yumi Kawamura et Reiko Tanaka
Last Surprise de Shoji Meguro
\#
SFX :
Money kaching :
Gaming Sound FX
https://www.youtube.com/channel/UCi-xN4ZB6e-0JcXzvBEomlw
Jet de dés:
Sound Effects Factory
https://www.youtube.com/channel/UCYIxR_86Ck0sCL26eVuumvQ
\#
Voix :
Aigis de persona 3 par Karen Strassman
Yukari Takeba de persona 3 par Michelle Ruff
Chie Satonaka de persona 4 par Erin Fitzgerald
Yukiko Amagi de persona 4 par Amanda Winn Lee
Yosuke Hanamura de persona 4 par Yuri Lowenthal
Morgana de persona 5 par Cassandra Lee Morris
Ryuji Sakamoto de persona 5 par Max Mittelman
Futaba Skura de persona 5 par Erica Lindbeck