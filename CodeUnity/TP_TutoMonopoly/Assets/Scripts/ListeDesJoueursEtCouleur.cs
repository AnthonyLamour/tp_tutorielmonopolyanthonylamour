﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListeDesJoueursEtCouleur : MonoBehaviour
{
    //liste des joueurs et de leurs couleurs

    public static string couleurJ1 = "blanc";

    public static string couleurJ2 = "bleu";

    public static string couleurJ3 = "gris";

    public static string couleurJ4 = "marron";

    public static string couleurJ5 = "orange";

    public static string couleurJ6 = "rouge";

    public static string couleurJ7 = "vert";

    public static string couleurJ8 = "violet";
}
